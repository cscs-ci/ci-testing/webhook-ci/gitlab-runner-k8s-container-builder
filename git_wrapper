#!/bin/bash -l

set -e

if [[ ! -f /tmp/ssh-agent.env ]] ; then
    MY_IP="$(curl --retry 10 --retry-connrefused --retry-delay 30 --retry-all-errors --no-progress-meter ip.me)"
    echo "Executing on $MY_IP"
    ssh-agent > /tmp/ssh-agent.env
    source /tmp/ssh-agent.env >/dev/null
    if [[ ! -v CSCS_CI_MW_URL ]] ; then
        echo "Variable CSCS_CI_MW_URL is unset, this is an error"
        exit 1
    fi
    if [[ -z "$CSCS_CI_MW_URL" ]] ; then
        echo "Variable CSCS_CI_MW_URL is empty, this is an error"
        exit 1
    fi
    curl --retry 10 --retry-connrefused -s "${CSCS_CI_MW_URL}/credentials?token=${CI_JOB_TOKEN}&job_id=${CI_JOB_ID}&creds=sshkey" | sed -e 's/.*"\(.*\)"}}/\1/' -e 's/\\n/\n/g'| ssh-add - >/dev/null 2>&1

    # send job data to middleware
    # please note that no data will be sent to middleware if `GIT_FETCH: none` variable is set. However this git_wrapper script
    # is still the best place to do it, because before_script and script could be overwritten by users, while this script is always
    # called as long as GIT_FETCH != none. Also this script is the only script that has all variables set, that are needed.
    # The build container in the pod has almost all variables set, but some are missing (e.g. CSCS_CI_MW_URL, which is injected via middleware directly), however the generated script get_sources includes
    # that variable again, hence we can use it here
    if [[ "${SEND_JOB_METADATA}" == "YES" ]] ; then
        curl --retry 10 --retry-connrefused -s -H 'Content-Type: application/json' --data-raw '{"mode":"container-lightweight"}' "${CSCS_CI_MW_URL}/redis/job?token=${CI_JOB_TOKEN}&job_id=${CI_JOB_ID}"
    fi
else
    source /tmp/ssh-agent.env >/dev/null
fi

if [[ "$@" =~ " --depth " ]] ; then
    if ! /usr/bin/git "$@" ; then
        /usr/bin/git "$@" --depth 10000
    fi
else
    exec /usr/bin/git "$@"
fi
